import Vue from 'vue'
import App from './App'
Vue.config.productionTip = false

// 库
App.mpType = 'app'
import verify from './common/verify/index.js'
Vue.prototype.$verify = verify
import pub from './common/public/index.js'
Vue.prototype.$pub = pub
import tool from './common/Tools/index.js'
Vue.prototype.$tool = tool





// 接入环信IM
// import HuanxinIM from './common/HuanXinSdk/index.js'
import HuanxinIM from './common/HuanXinSdk/HunXinIM.js'
let $HuanxinIM = new HuanxinIM();
Vue.prototype.$HuanxinIM = $HuanxinIM;



// 自动登录环信IM 
let HuanXinInfo = pub.getStorageSync('HuanXinInfo');
if(HuanXinInfo){
	$HuanxinIM.Login({
		user:pub.getStorageSync('UserInfo')._id,
		accessToken:HuanXinInfo.access_token
	})
}else{
	// pub.redirTo('/pages/other/login/index')
}


// #ifdef APP-PLUS
import checkUpdate from './common/components/update/yang-update.js'
setTimeout(()=>{checkUpdate()},1000)
// #endif

// 全局组件
import AppHeaders from './common/components/AppHeader/index.vue'
Vue.component('app-headers', AppHeaders)

// 引入全局uView
import uView from 'uview-ui'
Vue.use(uView)


import * as filters from './common/filter.js'
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})


import store from './store/index.js' 
Vue.prototype.$store = store
Vue.mixin({
	// watch:{
	// 	'CurrentTheme.name'(oldVal,newVal){
	// 		// 设置导航栏字体前景色
	// 		this.newVal==='DarkMode'?plus.navigator.setStatusBarStyle("light"):plus.navigator.setStatusBarStyle("dark")
	// 		// console.log(666,oldVal,newVal)
	// 	}
	// },
	computed: {
		CurrentTheme() {
			return this.$store.getters.Get_CurrentMode
		}
	}
})

const app = new Vue({
    ...App
})
app.$mount()
