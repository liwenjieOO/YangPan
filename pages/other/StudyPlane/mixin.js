// 传入当前的日期，返回当天/周/月/自定义范围所以的打卡记录
let CurrentDayRecord = (dates, type) => {
	// 类型判断  other是周/月
	let data1 = type==='other' ? dates.split('~')[0] :"";
	let data2 = type==='other' ?dates.split('~')[1] :"";
	
	let listData = uni.getStorageSync('StudyPlane');
	let returnJson = []
	listData.forEach(i => {
		i.Child.forEach(ii => {
			let CurrentDateTimeStart = type==='other' ? new Date(`${data1} 00:00:00`).getTime() : new Date(`${dates} 00:00:00`).getTime() 
			let CurrentDateTimeEnd = type==='other' ? new Date(`${data2} 23:59:59`).getTime() : new Date(`${dates} 23:59:59`).getTime()
		
			if (CurrentDateTimeStart <= ii.OpearteTime && CurrentDateTimeEnd >= ii.OpearteTime) {
				let ChildArr = ii.StudyTime.split('.').map(Number);
				let findIndexSSS = returnJson.findIndex(item => item.name === i.title)
				// 如果存在就累加，不存在就添加
				if (findIndexSSS === -1) {
					returnJson.push({
						name: i.title,
						data: (ChildArr[0] * 60 + ChildArr[1])
					})
				} else {
					returnJson[findIndexSSS].data += (ChildArr[0] * 60 + ChildArr[1])
				}
			}
		})
	})
	returnJson.forEach(i => {
		i.name += "(" + Math.floor(i.data / 60) + '小时' + i.data % 60 + '分钟' + ")"
	})
	return JSON.parse(JSON.stringify(returnJson))

}
export default {
	CurrentDayRecord
}
