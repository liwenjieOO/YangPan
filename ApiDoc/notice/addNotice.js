/**
 * @api {post} /YangPan 1.addNotice
 * @apiName addNotice
 * @apiGroup Notice
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 添加通知信息。
 * 
 * 
 * 
 * @apiParam {String} type 				示例:{type:'addNotice'}
 * @apiParam {String} noticeUserId      要被通知的用户id
 * @apiParam {String} noticeArticleId   被点赞/收藏的文章id
 * @apiParam {String} noticeCommonId   	被评论的评论id
 * @apiParam {Number} noticeType   		通知类型
 * 										1:点赞
 *										2:评论(要加上noticeCommonId)
 *										3:关注
 *										4:发不新文章
 *										5.收藏
 *										6.分享
 * 										7.评论点赞（要加上noticeCommonId）
 * @apiParam {String} SourceUserId      触发这个通知的用户
 * 
 * 
 * 
 * @apiSuccess {String}  test 数据待填.
 */
