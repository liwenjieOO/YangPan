/**
 * @api {post} /YangPan 2.DelNotice
 * @apiName DelNotice
 * @apiGroup Notice
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 删除通知信息。
 * 
 * 
 * 
 * @apiParam {String} type 		示例:{type:'DelNotice'}
 * @apiParam {String} _id     	该条记录的_id
 * 
 * 
 * 
 * @apiSuccess {String}  test 数据待填.
 */
