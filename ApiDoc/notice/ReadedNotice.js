/**
 * @api {post} /YangPan 3.ReadedNotice
 * @apiName ReadedNotice
 * @apiGroup Notice
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 完成阅读通知。
 * 
 * 
 * 
 * @apiParam {String} type 		示例:{type:'ReadedNotice'}
 * @apiParam {String} _id     	该条记录的_id
 * 
 * 
 * 
 * @apiSuccess {String}  test 数据待填.
 */
