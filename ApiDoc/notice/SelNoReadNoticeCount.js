/**
 * @api {post} /YangPan 4.SelNoReadNoticeCount
 * @apiName SelNoReadNoticeCount
 * @apiGroup Notice
 *
 *
 * 
 * @apiDescription 接口说明：
 * 查询通知未读信息统计
 * 
 * 
 * 
 *  * @apiParam {String} type 		   示例:{type:'SelNoReadNoticeCount'}
 * 
 * 
 * @apiSuccess {Number}  total 未读信息数.
 */
