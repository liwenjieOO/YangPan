/**
 * @api {post} /YangPan 5.SelNotice
 * @apiName SelNotice
 * @apiGroup Notice
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 查询通知信息
 * 
 * 
 * 
 * @apiParam {String} type 		   示例:{type:'SelNotice'}
 * @apiParam {Number} pageSize     分页大小
 * @apiParam {Number} pageNum   页码
 * @apiParam {Number} noticeType   通知类型
 * 										1:点赞
 *										2:评论
 *										3:关注
 *										4:发不新文章
 *										5.收藏
 *										6.分享
 * 
 * 
 * 
 * @apiSuccess {String}  test 数据待填.
 */
