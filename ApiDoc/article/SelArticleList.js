/**
 * @api {post} /YangPan 3.SelArticleList
 * @apiName SelArticleList
 * @apiGroup community
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 获取文章列表。
 * 
 *
 * 
 * @apiParam {String} type 			示例:{type:'SelArticleList'}
 * @apiParam {String} [UserId]     	这个我前端个根本不用传的，后端拿token就能拿到
 * @apiParam {Number} pageSize     	分页大小
 * @apiParam {Number} pageNum   	页码
 * 
 * 
 * @apiSuccess {String}  test 数据待填.
 */
