/**
 * @api {post} /YangPan 2.DelArticle
 * @apiName DelArticle
 * @apiGroup community
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 删除文章。
 * 
 *
 * 
 * @apiParam {String} type 			示例:{type:'DelArticle'}
 * @apiParam {String} articleId     文章id
 * 
 * 
 * @apiSuccess {String}  test 数据待填.
 */
