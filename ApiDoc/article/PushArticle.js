/**
 * @api {post} /YangPan 1.PushArticle
 * @apiName PushArticle
 * @apiGroup community
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 发布文章。
 * 
 *
 * 
 * @apiParam {String} type 			示例:{type:'PushArticle'}
 * @apiParam {String} _id      		发布者的用户ID
 * @apiParam {String} [contents]   	发布内容
 * @apiParam {Array}  [imgList]   	发布图片数组内容
 * @apiParam {Object} [audioList]   发布语音对象内容
 * @apiParam {Object} [videoList]   发布视频对象内容
 * @apiParam {String} PushType      发布类型  'img/text' || 'audio/text' || 'video/text'
 * 
 * 
 * @apiSuccess {String}  test 数据待填.
 */
