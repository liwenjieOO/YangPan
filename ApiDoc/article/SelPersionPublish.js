/**
 * @api {post} /YangPan 5.SelPersionPublish
 * @apiName SelPersionPublish
 * @apiGroup community
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 查询个人发布。
 * 
 *
 * 
 * @apiParam {String} type 			示例:{type:'SelPersionPublish'}
 * @apiParam {Number} pageSize     	分页大小
 * @apiParam {Number} pageNum   	页码
 * @apiParam {String} PersionId     要查询的用户id
 * @apiParam {String} [UserId]     	这个我前端个根本不用传的，后端拿token就能拿到(点赞信息的)
 * 
 * 
 * @apiSuccess {String}  test 数据待填.
 */
