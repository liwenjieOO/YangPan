/**
 * @api {post} /YangPan 7.UploadRecord
 * @apiName UploadRecord
 * @apiGroup community
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 上传文件记录。
 * 
 *
 * 
 * @apiParam {String} type 			示例:{type:'UploadRecord'}
 * @apiParam {String} fileID     	文件id
 * @apiParam {String} UploadType   	上传类型
 * @apiParam {Number} [duration]    录音秒数
 * 
 * 
 * @apiSuccess {String}  test 数据待填.
 */
