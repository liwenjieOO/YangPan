/**
 * @api {post} /YangPan 6.SelArticle_like
 * @apiName SelArticle_like
 * @apiGroup community
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 文章的模糊搜索。
 * 
 *
 * 
 * @apiParam {String} type 			示例:{type:'SelArticle_like'}
 * @apiParam {String} content     	要模糊查询的内容
 * @apiParam {Number} pageSize     	分页大小
 * @apiParam {Number} pageNum   	页码
 * 
 * 
 * @apiSuccess {String}  test 数据待填.
 */
