/**
 * @api {post} /YangPan 4.SelArticleDetail
 * @apiName SelArticleDetail
 * @apiGroup community
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 查询当前文章详细信息。
 * 
 *
 * 
 * @apiParam {String} type 			示例:{type:'SelArticleDetail'}
 * @apiParam {String} articleId     文章id
 * @apiParam {String} [UserId]     	这个我前端个根本不用传的，后端拿token就能拿到
 * 
 * 
 * @apiSuccess {String}  test 数据待填.
 */
