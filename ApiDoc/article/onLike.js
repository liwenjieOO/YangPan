/**
 * @api {post} /YangPan 8.onLike
 * @apiName onLike
 * @apiGroup community
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 文章点赞接口。
 * 
 *
 * 
 * @apiParam {String} type 			示例:{type:'onLike'}
 * @apiParam {String} _id     		文章id
 * 
 * 
 * 
 * 
 * @apiSuccess {String}  test 数据待填.
 */
