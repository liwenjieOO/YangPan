/**
 * @api {post} /YangPan 7.APIUpdatePwd
 * @apiName APIUpdatePwd
 * @apiGroup User
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 修改个人密码(不需要鉴权)
 * 
 * 
 * 
 * @apiParam {String} type 				示例:{type:'APIUpdatePwd'}
 * @apiParam {String} account      	 	账号
 * @apiParam {String} newPwd   			新密码（需要加密）
 * 
 * 
 * 
 * @apiSuccess {Number}  code:200 修改成功~
 * @apiSuccess {Number}  code:204 参数错误~
 * 
 * 
 * 
 * @apiError {Number} code:500 内部错误
 */
