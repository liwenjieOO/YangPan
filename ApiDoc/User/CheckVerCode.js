/**
 * @api {post} /YangPan 6.CheckVerCode
 * @apiName CheckVerCode
 * @apiGroup User
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 校验验证码是否正确
 * 
 * 
 * 
 * @apiParam {String} type 				示例:{type:'CheckVerCode'}
 * @apiParam {String} account      	 	账号
 * @apiParam {String} verCode   		验证码
 * 
 * 
 * 
 * @apiSuccess {Number}  code:200 验证成功~
 * @apiSuccess {Number}  code:204 参数错误~ || 该验证码已过期 || 验证码错误 || 短信未发送
 * 
 * 
 * 
 * @apiError {Number} code:500 内部错误
 */
