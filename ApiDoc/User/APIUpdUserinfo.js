/**
 * @api {post} /YangPan 5.APIUpdUserinfo
 * @apiName APIUpdUserinfo
 * @apiGroup User
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 修改个人信息
 * 
 * 
 * 
 * @apiParam {String} type 				示例:{type:'APIUpdUserinfo'}
 * @apiParam {String} avatar      	 	头像地址
 * @apiParam {String} [Nickname]   		昵称
 * @apiParam {String} [sex]   			性别
 * @apiParam {String} [birth]   		生日
 * @apiParam {String} [Region]   		地区
 * @apiParam {String} [introduce]   	个人简介
 * 
 * 
 * 
 * @apiSuccess {Number}  code:200 修改成功~
 * @apiSuccess {Number}  code:204 参数错误~
 * 
 * 
 * 
 * @apiError {Number} code:500 内部错误
 */
