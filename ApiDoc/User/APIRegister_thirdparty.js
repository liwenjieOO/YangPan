/**
 * @api {post} /YangPan 2.APIRegister_thirdparty
 * @apiName APIRegister_thirdparty
 * @apiGroup User
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 第三方登录/注册接口。
 * 
 * 
 * 
 * @apiParam {String} type 			示例:{type:'APIRegister_thirdparty'}
 * @apiParam {String} way       	登录注册方式 例：qq/weixin/xinlang
 * @apiParam {String} openid   		唯一openid
 * @apiParam {String} [Nickname]   	昵称
 * @apiParam {String} [sex]   		性别
 * @apiParam {String} [avatar]   	头像地址
 * 
 * 
 * @apiSuccess {Number}  code:200 注册成功~ || 参数错误~
 * @apiSuccess {Number}  code:204 该账户已注册过~ || 该短信未发送~ || 验证码已过期，请重新获取~
 * 
 * 
 * 
 * @apiError {Number} code:500 内部错误
 */
