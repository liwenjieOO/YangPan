/**
 * @api {post} /YangPan 1.APIRegister
 * @apiName APIRegister
 * @apiGroup User
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 注册接口。
 * 
 * 
 * 
 * @apiParam {String} type 			示例:{type:'APIRegister'}
 * @apiParam {String} account       账号（手机号）
 * @apiParam {String} pwd   		密码
 * @apiParam {String} verCode   	验证码
 * 
 * 
 * 
 * @apiSuccess {Number}  code:200 注册成功~
 * @apiSuccess {Number}  code:204 该账户已注册过~ || 该短信未发送~ || 验证码已过期，请重新获取~
 * 
 * 
 * 
 * @apiError {Number} code:500 内部错误
 */
