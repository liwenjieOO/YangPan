/**
 * @api {post} /YangPan 4.APIGetUserinfo
 * @apiName APIGetUserinfo
 * @apiGroup User
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 获取个人信息。
 * 
 * 
 * 
 * @apiParam {String} type 			示例:{type:'APIGetUserinfo'}
 * @apiParam {String} null       	暂无参数
 * 
 * 
 * 
 * @apiSuccess {Number}  code:200 获取个人信息成功~
 * @apiSuccess {Number}  code:204 该用户未存在~ 
 * 
 * 
 * 
 * @apiError {Number} code:500 内部错误
 */
