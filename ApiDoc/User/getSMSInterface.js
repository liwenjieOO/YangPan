/**
 * @api {post} /YangPan 8.getSMSInterface
 * @apiName getSMSInterface
 * @apiGroup User
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 获取短信验证码(不需要鉴权)
 * 
 * 
 * 
 * @apiParam {String} type 				示例:{type:'getSMSInterface'}
 * @apiParam {String} SMSType      	 	短信类型
 * @apiParam {String} account   		手机号
 * 
 * 
 * 
 * @apiSuccess {Number}  code:200 修改成功~
 * @apiSuccess {Number}  code:204 参数错误~
 * 
 * 
 * 
 * @apiError {Number} code:500 内部错误
 */
