/**
 * @api {post} /YangPan 1.onCommon
 * @apiName onCommon
 * @apiGroup common
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 评论接口（一级和二级一起）。
 * 
 *
 * 
 * @apiParam {String} type 						示例:{type:'onCommon'}
 * @apiParam {String} articleId     			文章id
 * @apiParam {String} UserId    				用户id
 * @apiParam {String} commonValue   			评论内容
 * @apiParam {String} [parentId]      			被指定回复的_id(一级，注意这边是_id)
 * @apiParam {String} [Second_appointed_id]    	被指定回复的_id(二级，注意这边是_id)(目前没用上)（二级评论指定回复那个二级评论的数据_id）
 * @apiParam {String} [reply_UseId]     		被指定回复的UserId(二级，注意这边是UserId)（二级评论指定回复那个二级评论的用户id）
 * 		
 * 
 * @apiSuccess {String}  test 数据待填.
 */
