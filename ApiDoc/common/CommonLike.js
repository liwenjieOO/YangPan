/**
 * @api {post} /YangPan 3.CommonLike
 * @apiName CommonLike
 * @apiGroup common
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 单条评论点赞。
 * 
 *
 * 
 * @apiParam {String} type 				示例:{type:'CommonLike'}
 * @apiParam {String} _id     			评论id
 * 		
 * 
 * @apiSuccess {String}  test 数据待填.
 */
