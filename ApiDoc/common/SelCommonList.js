/**
 * @api {post} /YangPan 2.SelCommonList
 * @apiName SelCommonList
 * @apiGroup common
 *
 * 
 * 
 * @apiDescription 接口说明：
 * 查询评论接口（一级二级都行）。
 * 
 *
 * 
 * @apiParam {String} type 						示例:{type:'SelCommonList'}
 * @apiParam {String} articleId     			文章id
 * @apiParam {String} pageSize    				用户id
 * @apiParam {String} pageNum   				评论内容
 * @apiParam {String} [parentId]    			二级评论的parentId
 * @apiParam {String} [SortType]     			排序类型： SortNew：按最新发布排序  | SortMoreLike：按最多点赞排序 | SortHot：按最热评论排序
 * 												
 * 		
 * 
 * @apiSuccess {String}  test 数据待填.
 */
