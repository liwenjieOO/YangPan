### 1.切换登录后 无刷新更新我的页面的 个人信息

​	~.登录成功后 存个人信息

```js
this.$pub.setStorageSync('UserInfo', res1.result);
```

​	~ .在我的页面下onShow下做无刷新更新

```js
onShow() {
		if (this.$pub.getStorageSync('UserInfo')) {
			this.UserInfo.Nickname = uni.getStorageSync('UserInfo').Nickname || '暂未设置';
			this.UserInfo.avatar = uni.getStorageSync('UserInfo').avatar || 'http://pic2.sc.chinaz.com/Files/pic/pic9/202002/hpic2119_s.jpg';
		}
	},
```

​	~.在我的页面下第一次加载还是申请接口 存本地缓存

### 2.切换登录后 更新文件模块的接口 逻辑不同于我的页面 只在账户切换后刷新

在file页面下定义全局变量

```js
let CurrentAccout = null; // 当前的使用账户，用户切换用户的判断刷新
```

![image-20201008175313653](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20201008175313653.png)

在created先赋值  然后在onShow每次对比身份信息的_id 不同就刷新文件列表接口

```js
async created() {
    await this.GetFolderList();
    CurrentAccout = this.$pub.getStorageSync('UserInfo')._id
},
onShow() {
    uni.showTabBar({ animation: true });

    // 切换账户下 更新文件夹 逻辑不同于我的页面 只在账户切换后刷新
    if (CurrentAccout !== this.$pub.getStorageSync('UserInfo')._id) {
        this.freshFolder()
    }

},
```



### 3.Sts权限是30分钟刷新一次

### 4.自定义触底加载

```js
// 监控评论节点的触底加载
commonBlockTouchmove(e) {
    let views = uni.createSelectorQuery().select('.commonBlock');
    views
        .boundingClientRect(data => {
            // 44 + 10 = 54      750-54 = 696
            if (Math.floor(data.bottom) < 696) {
                this.LoadMoreStatus = 'loading';
                if (this.SelJson.pageNum + 1 > Math.floor(this.total / this.SelJson.pageSize)) return (this.LoadMoreStatus = 'nomore');
                this.SelJson.pageNum++;
                this.SelCommon();
                console.log('触底了');
            }
            // if(Math.floor(data.height) <=  Math.abs((Math.floor(data.top))) +  Math.floor(data.bottom)){}
        })
        .exec();
},
```

### 5.账号管理

登录时候添加账号缓存

​	先判断账号是否存在，是就覆盖，不是就直接添加

#### 登录界面的账号缓存判断

```js
// 取账号缓存
let oldAccountList = this.$pub.getStorageSync('AcclountList')
let indexS = oldAccountList ? oldAccountList.findIndex(i => i._id === res1.result._id) : -1
// 账号缓存不存在
if (!oldAccountList || indexS === -1) {
    oldAccountList = oldAccountList ? oldAccountList :[],
    oldAccountList ? oldAccountList.forEach(i=>i.IsCheck=false) :""
    let AccontJson = [{
        UpdateDate: new Date(),
        Token: res.Token,
        IsCheck:true ,// 当前选中的登录的账号
        Account: res1.result.account,
        Nickname: res1.result.Nickname,
        Avatar: res1.result.avatar,
        _id: res1.result._id,

        Info:res1.result
    }]
    this.$pub.setStorageSync('AcclountList', [...oldAccountList,...AccontJson]); // 添加账号缓存
// 账号缓存存在 覆盖掉旧数据
} else {
    oldAccountList.forEach(i=>i.IsCheck=false)

    console.warn('我是覆盖的逻辑', oldAccountList, oldAccountList[indexS])
    oldAccountList[indexS].UpdateDate = new Date();
    oldAccountList[indexS].Token = res.Token;
    oldAccountList[indexS].Nickname = res1.result.Nickname;
    oldAccountList[indexS].Avatar = res1.result.avatar;
    oldAccountList[indexS].IsCheck = true;

    oldAccountList[indexS].Info = res1.result;
    this.$pub.setStorageSync('AcclountList', [...oldAccountList]); // 添加账号缓存
}
```

#### 账号切换的页面逻辑

```js
// 选择账号
onCheck(e) {
    // 选中的是当前账号不做操作  || 编辑账号下不做操作
    if(this.$pub.getStorageSync('Token') === e.Token ||  this.navRightTxt === '完成') return


    /*
        本地切换
    */ 
    this.AccountList.forEach(i =>this.$set(i, 'IsCheck', false))
    this.$pub.setStorageSync('Token',e.Token)	// 替换token
    this.$pub.setStorageSync('UserInfo',e.Info) // 替换用户的个人信息
    this.$set(e, 'IsCheck', true)

    /*
        同步到缓存内
    */
    this.$pub.setStorageSync('AcclountList',this.AccountList)

    uni.showLoading({mask:true,title:'切换中'})
    setTimeout(()=>{
        uni.hideLoading()
        this.$pub.reLaunch('/pages/index/index')
    },1000)

},
// 退出当前账号
onExitAccount() {
    this.YModalTxt = '是否退出该账号?'
    this.YModalShow = true
},
// 弹窗成功回调
YModal_SubmitOk(e) {
    if (this.YModalTxt === '是否删除该账号信息？') {
        let DelInfo = JSON.parse(JSON.stringify( this.AccountList[this.CurrentSelDelAccountID]))
        // 静态修改
        this.AccountList.splice(this.CurrentSelDelAccountID, 1) //删除

        // 当只剩一个账号缓存的时候
        if(this.AccountList.length<1){
            this.$pub.delStorageSync('Token')		// 清空操作
            this.$pub.delStorageSync('UserInfo') 	// 清空操作
            this.$pub.delStorageSync('AcclountList')// 清空操作
            this.$pub.delStorageSync('Sts')			// 清空操作
            this.$pub.reLaunch('/pages/other/login/index')
            return
        }

        // 如果删除是当前的账号那么切换到第一个账号
        if(DelInfo.Token === this.$pub.getStorageSync('Token')){
            this.AccountList[0].IsCheck = true // 切换到第一个账号
            this.$pub.setStorageSync('Token',this.AccountList[0].Token)	// 替换token
            this.$pub.setStorageSync('UserInfo',this.AccountList[0].Info) // 替换用户的个人信息
            this.editIcon = !this.editIcon
            this.navRightTxt = '编辑'
        }

        // 同步到缓存内
        this.$pub.setStorageSync('AcclountList',this.AccountList)
    }else if (this.YModalTxt === '是否退出该账号?') {
        let indexS = this.AccountList.findIndex(i => i.Token == this.$pub.getStorageSync('Token'))
        this.AccountList.splice(indexS, 1) //删除
        this.$pub.delStorageSync('Token')		// 清空操作
        this.$pub.delStorageSync('UserInfo') 	// 清空操作
        this.$pub.delStorageSync('Sts')			// 清空操作
        this.AccountList.length!==0 ? this.$pub.setStorageSync('AcclountList',this.AccountList) :this.$pub.delStorageSync('AcclountList')
        this.$pub.reLaunch('/pages/other/login/index')
    }
},
// 删除账号缓存
onDelAccont(e) {
    this.YModalTxt = '是否删除该账号信息？'
    this.YModalShow = true
    this.CurrentSelDelAccountID = this.AccountList.findIndex(i => i._id == e._id)
}
```

#### 请求拦截token过期的

```js
// 删除账号缓存
let AccountList = $pub.getStorageSync('AcclountList')
let indexS = AccountList.findIndex(i => i.Token == $pub.getStorageSync('Token'))
AccountList.splice(indexS,1)
$pub.setStorageSync('AcclountList',AccountList)
```

### 6.聊天管理

#### 1.登录

##### 账号密码登录

我这边登录页登录的是用账号密码登录

​	账号是洋盘登录后返回的个人信息内的_id字段

​	密码是通过MD5加密的密码（注意用户修改密码的同时也要修改下IM的密码）

```js
try{
    this.$HuanxinIM.close()
}catch(e){
    console.log('初始化登录IM',e)
}
console.log('登录IM')
// 登录IM
let {msg,data} = await this.$HuanxinIM.Login({
    user:res1.result._id,
    pwd: MD5(this.pwd)
})
this.$pub.setStorageSync('HuanXinInfo',data)
```

##### 验证码登录

```js

```



##### 第三方登录

​	第三方登录账号是_id

​	密码是用openID

```js
// 环信IM登录
try{
    this.$HuanxinIM.close()
}catch(e){
    console.log('初始化登录IM',e)
}finally{
    setTimeout(async()=>{
        console.log('登录IM')
        // 登录IM
        let {msg,IM_data} = await this.$HuanxinIM.Login({
            user:res.data._id,
            pwd: MD5(options.openid)
        })
        if(msg==='fail'){
            this.$pub.Toast('账号密码错误~',IM_data)
            this.$pub.reLaunch('/pages/other/login/index')
        }
        console.log(IM_data)
        this.$pub.setStorageSync('HuanXinInfo',IM_data)
    },2500)
}
```



#### 2.token机制自动登录

我这边如果登录过了会存下 登录环信成功后返回的info，info内有access_token

这边自动登录就用access_token登录（在main.js自动登录）

```js
// 自动登录环信IM 
if(this.$pub.getStorageSync('HuanXinInfo')){
    this.$HuanxinIM.Login({
        user:this.$pub.getStorageSync('UserInfo')._id,
        accessToken:this.$pub.getStorageSync('HuanXinInfo').access_token,
    })
}else{
    this.$pub.redirTo('/pages/other/login/index')
}
```

#### 3.退出账号

我退出账号会清空掉这个账号缓存，并跳转登录页面，并断开连接（是reLaunch哦）

```js
this.$pub.delStorageSync('Token')				// 清空操作
this.$pub.delStorageSync('UserInfo') 			// 清空操作
this.$pub.delStorageSync('Sts')					// 清空操作
this.$pub.delStorageSync('HuanXinInfo')			// 清空操作
this.$HuanxinIM.close()							// 断开连接
```

#### 4.添加账号

跳转到登录界面

```js
try{
    this.$HuanxinIM.close()
}catch(e){
    console.log('初始化登录IM',e)
}finally{
    setTimeout(async()=>{
        console.log('登录IM')
        // 登录IM
        let {msg,data} = await this.$HuanxinIM.Login({
            user:res1.result._id,
            pwd: MD5(this.pwd)
        })
        if(msg==='fail'){
            this.$pub.Toast('账号密码错误~',data)
            this.$pub.reLaunch('/pages/other/login/index')
        }
        console.log(data)
        this.$pub.setStorageSync('HuanXinInfo',data)
    },2500)
}
```



#### 5.删除账号（未写）

#### 6.切换账号

```js
let _id = this.$pub.getStorageSync('UserInfo')._id
let Res_SelPwd = await API$SelPwd({_id})
this.$HuanxinIM.close()
uni.showLoading({mask:true,title:'切换中'})
setTimeout( async()=>{
    uni.hideLoading()
    // 重新登录IM  (pwd/openid)
    let {msg,data} = await this.$HuanxinIM.Login({
        user:_id,
        pwd: MD5(Res_SelPwd.data.pwd || Res_SelPwd.data.openid)
    })
    if(msg==='fail'){
        this.$pub.Toast('账号密码错误~',data)
        this.$pub.reLaunch('/pages/other/login/index')
    }
    this.$pub.setStorageSync('HuanXinInfo',data)
    this.$pub.reLaunch('/pages/index/index')
},1500)
```

#### 7.关于注册



环信断线重连

app内的storage是无大小限制的，准备所有的聊天记录存本地



发送信息

​	在本地缓存内添加一个以用户名字（数据库的_id）命令的键值对

​		每条内容都缓存在键值对内的一个字段内

登录后接受消息

```
由于环信给我返回的数据是只有to这个字段
	那么如果是之前有这个聊天记录的缓存 直接添加到chatList即可
	如果是全新的聊天那么，接收消息的时候要添加新的缓存
```

