import Server from "./utils/request.js"


export const API$login = (data)=>{
	let R = Object.assign({type: 'APILogin'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

// 获取短信验证码
export function API$getSMSInterface(data) {
	let R = Object.assign({type: 'getSMSInterface'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

// 短信验证码校验（用于重置密码）
export function API$CheckVerCode(data) {
	let R = Object.assign({type: 'CheckVerCode'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

// 注册接口
export function API$register(data) {
	let R = Object.assign({type: 'APIRegister'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

// 注册接口（第三方注册）
export function API$register_thirdparty(data) {
	let R = Object.assign({type: 'APIRegister_thirdparty'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

// 创建文件夹接口
export function API$NewFolder(data) {
	let R = Object.assign({type: 'APINewFolder'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

// 上传图片接口(获取签证)
export function API$UploadImg(data) {
	let R = Object.assign({type: 'APIUploadImg'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}
// 上传图片同步接口(成功后同步数据)
export function API$UploadImgSync(data) {
	let R = Object.assign({type: 'APIUploadImgSync'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

