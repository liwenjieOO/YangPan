import Server from "./utils/request.js"

// 获取个人信息
export function API$GetUserinfo(data) {
	let R = Object.assign({type: 'APIGetUserinfo'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

// 修改个人信息
export function API$UpdUserinfo(data) {
	let R = Object.assign({type: 'APIUpdUserinfo'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

// 修改个人头像
export function API$UpdUserAva(data) {
	let R = Object.assign({type: 'APIUpdUserAva'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}


// 查询个人发布
export const API$SelPersionPublish = (data)=>{
	let R = Object.assign({type: 'SelPersionPublish'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

// 查询历史更新记录
export const API$SelUpdateList = (data)=>{
	let R = Object.assign({type: 'SelUpdateList'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}


//修改密码
export const API$UpdatePwd = (data)=>{
	let R = Object.assign({type: 'APIUpdatePwd'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

