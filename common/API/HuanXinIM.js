import Server from "./utils/request.js"


// 通过_id号查询账号昵称和头像
export const API$SelNickNameAvar = (data)=>{
	let R = Object.assign({type: 'SelNickNameAvar'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

// 通过_id号查询账号密码
export const API$SelPwd = (data)=>{
	let R = Object.assign({type: 'SelPwd'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}
