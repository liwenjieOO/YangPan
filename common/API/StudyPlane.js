import Server from "./utils/request.js"

// 同步数据到云端
export const API$AsyncStyudyPlaneData = (data) => {
	let R = Object.assign({
		type: 'AsyncStyudyPlaneData'
	}, data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data: R
	})
}
// 删除计划条(整条)
export const API$DelStyudyPlaneData = (data) => {
	let R = Object.assign({
		type: 'DelStyudyPlaneData'
	}, data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data: R
	})
}
