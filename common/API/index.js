import Server from "./utils/request.js"

export const API$login = (data)=>{
	return Server({
		url: 'http://47.114.114.84:5333/api/login',
		method: 'post',
		data
	})
}

//  检查版本更新
export function CheckUpdate(data) {
	const R = Object.assign({
		type: 'CheckUpdate'
	}, data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data: R
	})
}

//  查看指定类型的全部文件 分页
export function getSpecifyTypeFile(data) {
	const R = Object.assign({
		type: 'getSpecifyTypeFile'
	}, data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data: R
	})
}
