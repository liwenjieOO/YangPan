import Server from "./utils/request.js"


// 查询通知信息
export const API$SelNotice = (data)=>{
	let R = Object.assign({type: 'SelNotice'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

// 阅读通知
export const API$ReadedNotice = (data)=>{
	let R = Object.assign({type: 'ReadedNotice'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

// 查询未读信息数量
export const API$SelNoReadNoticeCount = (data)=>{
	let R = Object.assign({type: 'SelNoReadNoticeCount'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}

// 删除通知信息
export const API$DelNotice = (data)=>{
	let R = Object.assign({type: 'DelNotice'},data)
	return Server({
		url: 'https://qddscxy-16b8d1.service.tcloudbase.com/YangPan',
		method: 'post',
		data:R
	})
}



