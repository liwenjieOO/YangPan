// 检验手机号码
const PhoneFormat = (value) => /^1[3456789]\d{9}$/.test(value)

//8到16位数字与字母组合
const pwdFormat = (value) => /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$/.test(value)

// 验证验证码(6位)
const verCodeFormat_6 = (value) => /^[0-9]{6}/.test(value)

// 校验邮箱
const EmailFormat = (value) =>
	/^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/.test(value)
// 社会信用代码
const SocialCreditFormat = (value) => /^[^_IOZSVa-z\W]{2}\d{6}[^_IOZSVa-z\W]{10}$/g.test(value)
export default {
	PhoneFormat,
	pwdFormat,
	EmailFormat,
	SocialCreditFormat,
	verCodeFormat_6
}
