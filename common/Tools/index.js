// 生成token
let create_token = function(leng) {
	leng = leng == undefined ? 32 : leng; //如果没设置token长度自动为32位
	let chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz23456789';
	let token = '';
	for (let i = 0; i < leng; ++i) {
		token += chars.charAt(Math.floor(Math.random() * chars.length))
	}
	return token ///返回之前使用md5加密一下
};

// 生成按日期名字
let create_timeName = function() {
	let obj = new Date();
	let D = {};
	D['year'] = obj.getFullYear();
	D['month'] = (obj.getMonth() + 1) >= 10 ? (obj.getMonth() + 1) : "0" + (obj.getMonth() + 1);
	D['date'] = obj.getDate() >= 10 ? obj.getDate() : "0" + obj.getDate();
	D['hour'] = obj.getHours() >= 10 ? obj.getHours() : "0" + obj.getHours();
	D['minute'] = obj.getMinutes() >= 10 ? obj.getMinutes() : "0" + obj.getMinutes();
	D['second'] = obj.getSeconds() >= 10 ? obj.getSeconds() : "0" + obj.getSeconds();
	return '' + D.year + D.month + D.date + D.hour + D.minute + D.second
};

// 生成时间
let create_time = function(obj, type) {
	let D = {};
	var type = type || 'YYYYMMMDD';
	var sign = sign || '-';
	Object.defineProperties(D, {
		'year': {
			value: null,
			writable: true
		},
		'month': {
			value: null,
			writable: true
		},
		'date': {
			value: null,
			writable: true
		},
		'hour': {
			value: null,
			writable: true
		},
		'minute': {
			value: null,
			writable: true
		},
		'second': {
			value: null,
			writable: true
		},
	});
	D['year'] = obj.getFullYear();
	D['month'] = (obj.getMonth() + 1) >= 10 ? (obj.getMonth() + 1) : "0" + (obj.getMonth() + 1);
	D['date'] = obj.getDate() >= 10 ? obj.getDate() : "0" + obj.getDate();
	D['hour'] = obj.getHours() >= 10 ? obj.getHours() : "0" + obj.getHours();
	D['minute'] = obj.getMinutes() >= 10 ? obj.getMinutes() : "0" + obj.getMinutes();
	D['second'] = obj.getSeconds() >= 10 ? obj.getSeconds() : "0" + obj.getSeconds();
	if (type == 'YYYYMMMDD') {
		return "" + D.year + sign + D.month + sign + D.date
	}
	if (type == 'HHMMSS') {
		return "" + D.hour + ":" + D.minute + ":" + D.second
	}
	if (type == 'YYYYMMMDDHHMMSS') {
		return "" + D.year + sign + D.month + sign + D.date + " " + D.hour + ":" + D.minute + ":" + D.second
	}
};

// 格式化时间
let formatDate = function(obj, type) {
	let D = {};
	var type = type || 'YYYYMMMDDHHMMSS';
	var sign = sign || '-';
	Object.defineProperties(D, {
		'year': {
			value: null,
			writable: true
		},
		'month': {
			value: null,
			writable: true
		},
		'date': {
			value: null,
			writable: true
		},
		'hour': {
			value: null,
			writable: true
		},
		'minute': {
			value: null,
			writable: true
		},
		'second': {
			value: null,
			writable: true
		},
	});
	D['year'] = obj.getFullYear();
	D['month'] = (obj.getMonth() + 1) >= 10 ? (obj.getMonth() + 1) : "0" + (obj.getMonth() + 1);
	D['date'] = obj.getDate() >= 10 ? obj.getDate() : "0" + obj.getDate();
	D['hour'] = obj.getHours() >= 10 ? obj.getHours() : "0" + obj.getHours();
	D['minute'] = obj.getMinutes() >= 10 ? obj.getMinutes() : "0" + obj.getMinutes();
	D['second'] = obj.getSeconds() >= 10 ? obj.getSeconds() : "0" + obj.getSeconds();
	if (type == 'YYYYMMMDD') {
		return "" + D.year + sign + D.month + sign + D.date
	}
	if (type == 'HHMMSS') {
		return "" + D.hour + ":" + D.minute + ":" + D.second
	}
	if (type == 'YYYYMMMDDHHMMSS') {
		return "" + D.year + sign + D.month + sign + D.date + " " + D.hour + ":" + D.minute + ":" + D.second
	}
}

// 查询周范围
let returnWeekRang = function(data) {
	/*
		@data:String 	YYYY-MM-DD hh-mm-ss格式
	*/
	let D = data ? new Date(data).getTime() : new Date().getTime();
	let Days = data ? new Date(data).getDay() : new Date().getDay();
	let StartData = D - (Days - 1) * 1000 * 60 * 60 * 24;
	let EndData = D + (7 - Days) * 1000 * 60 * 60 * 24;
	
	let StartDataString = create_time(new Date(StartData))
	let EndDataString = create_time(new Date(EndData))
	return StartDataString + '~' + EndDataString
}
// 查询月范围
let returnMonthRang = function(data) {
	/*
		@data:String 	YYYY-MM-DD hh-mm-ss格式
	*/
	let D = data ? new Date(data) : new Date();
	let Year = D.getFullYear();
	let GetTimes = D.getTime();
	let MonthS = D.getMonth()+1;
	let DateS = D.getDate();
	let FullDateS = ([1, 3, 5, 7, 8, 10, 12].findIndex(i => i ===MonthS)) !== -1 ? 31 : 30
	
	let RunNian = ((Year % 4) == 0) && ((Year % 100) != 0) || ((Year % 400) == 0) ? 29 : 28
	MonthS === 2 ? FullDateS = RunNian : ""
	
	let StartData = GetTimes - (DateS - 1) * 1000 * 60 * 60 * 24;
	let EndData = GetTimes + (FullDateS - DateS) * 1000 * 60 * 60 * 24;
	
	let StartDataString = create_time(new Date(StartData))
	let EndDataString = create_time(new Date(EndData))
	return StartDataString + '~' + EndDataString
}

// ȡ�ļ���׺��
let getFileType = (str) => str.match(/[^\.]\w*$/)[0]

// 计算是包含某字符
let HasIncludeChar = (str, charS) => str.indexOf(charS)

// 计算文件大小
let comFileSize = (value) => {
	if (null == value || value == '') {
		return "0 Bytes";
	}
	var unitArr = new Array("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB");
	var index = 0,
		srcsize = parseFloat(value);
	index = Math.floor(Math.log(srcsize) / Math.log(1024));
	var size = srcsize / Math.pow(1024, index);
	//  保留的小数位数
	size = size.toFixed(2);
	return size + unitArr[index];
}

// 发布时间 几分钟前计算
const friendlyDate = (time) => {
	if (!time) {
		return ''
	}
	let ms = time - Date.now()
	let num
	let quantifier
	let suffix = '后'
	if (ms < 0) {
		suffix = '前'
		ms = -ms
	}

	if (ms < 60000) {
		return '刚刚'
	}
	const seconds = Math.floor((ms) / 1000)
	const minutes = Math.floor(seconds / 60)
	const hours = Math.floor(minutes / 60)
	const days = Math.floor(hours / 24)
	const months = Math.floor(days / 30)
	const years = Math.floor(months / 12)
	switch (true) {
		case years > 0:
			num = years
			quantifier = '年'
			break
		case months > 0:
			num = months
			quantifier = '月'
			break
		case days > 0:
			num = days
			quantifier = '天'
			break
		case hours > 0:
			num = hours
			quantifier = '小时'
			break
		case minutes > 0:
			num = minutes
			quantifier = '分钟'
			break
		default:
			num = seconds
			quantifier = '秒'
			break
	}
	return `${num}${quantifier}${suffix}`
}

const ImgTypes = (type) => ['png', 'jpg', 'gif', 'jpeg'].find(i => i === type)

const ViodeoTypes = (type) => ['mp4', 'avi', 'rmvb', 'mov', 'FLV', 'OGG', 'MOD'].find(i => i === type)

const AudioTypes = (type) => ['mp3', 'flac', 'cd', 'WAVE', 'AIFF', 'MPEG', 'MPEG-4', 'MIDI', 'WMA', 'RealAudio', 'VQF',
	'OggVorbis', 'AMR', 'APE', 'FLAC', 'AAC'
].find(i => i === type)


const DocTypes = (type) => ['docx', 'doc', 'txt'].find(i => i ===type)

const PPTTypes = (type) => ['ppt', 'pptm', 'pptx', 'potx', 'pot', 'ppxs', 'ppsm', 'ppam', 'ppa', 'odp'].find(i => i ===
	type)
const yasuoTypes = (type) => ['rar', 'zip', '7z', 'gz', 'bz', 'ace', 'uha', 'zpaq', 'uda'].find(i => i === type)

export default {
	create_token,
	create_timeName,
	create_time,
	formatDate,
	returnWeekRang,
	returnMonthRang,
	getFileType,
	HasIncludeChar,
	comFileSize,
	friendlyDate,
	ImgTypes,
	ViodeoTypes,
	AudioTypes,
	DocTypes,
	PPTTypes,
	yasuoTypes
}
