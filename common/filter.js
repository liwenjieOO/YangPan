// 计算文件夹显示的路径
export function FolderPathFormat(name) {
	let nameList = name ? name.split('/') : '网络错误';
	return nameList == '网络错误' ? '网络错误' : nameList[nameList.length - 2];
}

// 计算文件显示的路径
export function FielPathFormat(name) {
	let nameList = name ? name.split('/') : '网络错误';
	return nameList == '网络错误' ? '网络错误' : nameList[nameList.length - 1];
}

// 计算文件大小
export function FileSizeFormat(Size) {
	if (null == Size || Size == '') {
		return "0 Bytes";
	}
	var unitArr = new Array("Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB");
	var index = 0,
		srcsize = parseFloat(Size);
	index = Math.floor(Math.log(srcsize) / Math.log(1024));
	var size = srcsize / Math.pow(1024, index);
	//  保留的小数位数
	size = size.toFixed(2);
	return size + unitArr[index];
}

// 计算是否是图片资源
export function comIsImg(type, str) {
	if (type == 'img') {
		return this.$tool.getFileType(str) == 'png' || this.$tool.getFileType(str) == 'jpg' || this.$tool.getFileType(str) ==
			'gif';
	} else if (type == 'video') {
		return this.$tool.getFileType(str) == 'mp4' || this.$tool.getFileType(str) == 'avi' || this.$tool.getFileType(str) ==
			'mov' ||
			this.$tool.getFileType(str) == 'rmvb' || this.$tool.getFileType(str) == 'FLV' || this.$tool.getFileType(str) ==
			'OGG' ||
			this.$tool.getFileType(str) == 'MOD'
	} else if (type == 'audio') {
		return this.$tool.getFileType(str) == 'mp3' || this.$tool.getFileType(str) == 'flac'
	}
}
