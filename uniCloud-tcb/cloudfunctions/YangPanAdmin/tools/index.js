// 生成token
exports.create_token=function (leng){
    leng = leng==undefined?32:leng;	//如果没设置token长度自动为32位
    let chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz23456789';
    let token = '';
    for(let i=0;i<leng;++i){
        token+=chars.charAt(Math.floor(Math.random()*chars.length))
    }
    return token	///返回之前使用md5加密一下
};
// 生成按日期名字
exports.create_timeName=function (){
    let obj = new Date();
    let D = {};
    D['year'] = obj.getFullYear();
    D['month'] = (obj.getMonth() + 1)>=10?(obj.getMonth() + 1):"0"+(obj.getMonth() + 1);
    D['date'] = obj.getDate()>=10 ? obj.getDate():"0"+ obj.getDate();
    D['hour'] = obj.getHours()>=10 ? obj.getHours():"0"+ obj.getHours();
    D['minute'] = obj.getMinutes()>=10 ? obj.getMinutes():"0"+ obj.getMinutes();
    D['second'] = obj.getSeconds()>=10 ? obj.getSeconds():"0"+ obj.getSeconds();
    return '' +  D.year + D.month + D.date + D.hour + D.minute + D.second
};
// 生成时间
exports.create_time=function (obj, type){
    let D = {};
    var type = type || 'YYYYMMMDD';
    var sign = sign || '-';
    Object.defineProperties(D, {
        'year': {
            value: null,
            writable: true
        },
        'month': {
            value: null,
            writable: true
        },
        'date': {
            value: null,
            writable: true
        },
        'hour': {
            value: null,
            writable: true
        },
        'minute': {
            value: null,
            writable: true
        },
        'second': {
            value: null,
            writable: true
        },
    });
    D['year'] = obj.getFullYear();
    D['month'] = (obj.getMonth() + 1)>=10?(obj.getMonth() + 1):"0"+(obj.getMonth() + 1);
    D['date'] = obj.getDate()>=10 ? obj.getDate():"0"+ obj.getDate();
    D['hour'] = obj.getHours()>=10 ? obj.getHours():"0"+ obj.getHours();
    D['minute'] = obj.getMinutes()>=10 ? obj.getMinutes():"0"+ obj.getMinutes();
    D['second'] = obj.getSeconds()>=10 ? obj.getSeconds():"0"+ obj.getSeconds();
    if (type == 'YYYYMMMDD') {
        return "" + D.year + sign + D.month + sign + D.date
    }
    if (type == 'HHMMSS') {
        return "" + D.hour + ":" + D.minute + ":" + D.second
    }
    if (type == 'YYYYMMMDDHHMMSS') {
        return "" + D.year + sign + D.month + sign + D.date + " " + D.hour + ":" + D.minute + ":" + D.second
    }
};

// 获取文件后缀名
exports.getFileType = (str) => str.match(/[^\.]\w*$/)[0]