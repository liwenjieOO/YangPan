'use strict';
const querystring = require('querystring');
const fs = require('fs');

const {add,select} = require('./model/index.js');
const router = require('./router/index.js');
// 全部接口名称校验
var Router = [
	// 用户
	'Login','Register','GetUserInfo','GetSts',
	'admin_SelUserList','admin_DelUser','admin_UpdlUser',
	'SelUserList','UpdUser','DelUser',
	// 权限
	'SyncRouter','SelRouter','AddRole','GetRoleInfo','UpdRole','DelRole',
	'TestConsole','APICheckToken','AddVersion','SelVersionList','DelVersion','UpdVersion','UpdCurrentUse','DelOssFile',
	'SelArticleList','UpdArticle','DelArticle',
];
// Webgl接口 
var WebglAPI = ['ModelList','ModelDetail']
Router = Router.concat(WebglAPI)

// 无需token接口的
var NoToken = [
	'Login','Register',
	'TestConsole','AddVersion','SelVersionList','DelVersion','UpdVersion','UpdCurrentUse','SelUserList','UpdUser','DelOssFile','DelUser',
]
NoToken = NoToken.concat(WebglAPI)



const CheckRouter=(API)=>{
	let len = Router.filter(i=>i==API).length
	return len >=1? true:false
}
exports.main = async (event, context) => {
	if(!event.httpMethod){
		/*********************************************客户端调云函数基础用法*********************************************/ 
		const {type} = event;
		if(!CheckRouter(type)){return {msg:"接口不存在，请参考文档"}};
		
		function eventType(type) {this.type = type}
		eventType.prototype.APICheckUpdate = (d)=> APICheckUpdate(d)
		let router_init = new eventType(type)
		return router_init[type](event)
		
	}else{
		/*********************************************使用云函数URL调法**********************************************************/ 
		const {httpMethod,headers} = event
		if(httpMethod=='POST'){//POST接口
			const Token = headers.token || null
			// 数据处理
			const body_data = event.body? JSON.parse(event.body) :'error'  // string转一次
			// 接口类型校验
			let { type } = body_data
			if(!CheckRouter(type)){return {msg:"接口不存在，请参考文档"}}
			body_data['Token'] = Token
			
			if(NoToken.find(i=>i==type)===undefined){
				let IdentityCheck = await router.APICheckToken(Token)
				if(IdentityCheck.code == 211){ return {code:IdentityCheck.code,msg:IdentityCheck.msg} }
				body_data['UserInfo'] = IdentityCheck.result.data[0]
			}
			// 路由注册
			function eventType(type) {this.type = type}
			
			// 用户模块
			eventType.prototype.Login = (d)=> router.Login(d)							// 登录接口
			eventType.prototype.Register = (d)=> router.Register(d)						// 注册接口
			eventType.prototype.GetUserInfo = (d)=> router.GetUserInfo(d)				// 获取用户信息接口
			eventType.prototype.GetSts = (d)=> router.GetSts(d)							// 获取临时Sts权限
			eventType.prototype.admin_SelUserList = (d)=> router.admin_SelUserList(d)	// 获取用户列表		(admin用户)
			eventType.prototype.admin_DelUser = (d)=> router.admin_DelUser(d)			// 删除用户			(admin用户)
			eventType.prototype.admin_UpdlUser = (d)=> router.admin_UpdlUser(d)			// 更新用户信息		(admin用户)
			eventType.prototype.SelUserList = (d)=> router.SelUserList(d)				// 获取用户列表		(洋盘用户)
			eventType.prototype.UpdUser = (d)=> router.UpdUser(d)						// 更新用户信息		(洋盘用户)
			eventType.prototype.DelUser = (d)=> router.DelUser(d)						// 删除用户			(洋盘用户)
			
			// 权限模块
			eventType.prototype.SyncRouter = (d)=> router.SyncRouter(d)					// 同步路由
			eventType.prototype.SelRouter = (d)=> router.SelRouter(d)					// 查询路由
			eventType.prototype.AddRole = (d)=> router.AddRole(d)						// 添加角色信息
			eventType.prototype.UpdRole = (d)=> router.UpdRole(d)						// 修改角色信息
			eventType.prototype.DelRole = (d)=> router.DelRole(d)						// 删除角色
			eventType.prototype.GetRoleInfo = (d)=> router.GetRoleInfo(d)				// 获取角色信息
			
			// 测试服务端打印接口
			eventType.prototype.TestConsole = (Token)=> router.TestConsole(Token)
			// 检查Token是否存在或过期
			eventType.prototype.APICheckToken = (Token)=> router.APICheckToken(Token)
			// 添加版本
			eventType.prototype.AddVersion = (d)=> router.AddVersion(d)
			//获取版本列表
			eventType.prototype.SelVersionList = (d)=> router.SelVersionList(d)
			//删除版本
			eventType.prototype.DelVersion = (d)=> router.DelVersion(d)
			//更新版本
			eventType.prototype.UpdVersion = (d)=> router.UpdVersion(d)
			//指定当前使用的版本
			eventType.prototype.UpdCurrentUse = (d)=> router.UpdCurrentUse(d)
			

			// 根据文件名字删除-oss文件   （admin删除以用户头像）
			eventType.prototype.DelOssFile = (d)=> router.DelOssFile(d)
			// 获取文章列表
			eventType.prototype.SelArticleList = (d)=> router.SelArticleList(d)
			// 修改文章信息
			eventType.prototype.UpdArticle = (d)=> router.UpdArticle(d)
			// 修改文章信息
			eventType.prototype.DelArticle = (d)=> router.DelArticle(d)
			
			
			
			
			/**************WEBGL*****************/ 
			// 查询模型列表
			eventType.prototype.ModelList = (d)=> router.ModelList(d)
			// eventType.prototype.ModelDetail = (d)=> router.ModelDetail(d)
			
			
			let router_init = new eventType(type)
			return router_init[type](body_data)
			
		}else if(httpMethod=='GET'){//GET接口
			let {type} = event.queryStringParameters
			if(!CheckRouter(type)){return {msg:"接口不存在，请参考文档"}}
		}
	}
	
};
