


// 接口类型分模块化
const User = require('./User/index.js')
const Permission = require('./Permission/index.js')
const Update = require('./Update/index.js')
const Article = require('./Article/index.js')
const Other = require('./other/index.js')
const WEBGL = require('./WebGL.js')




/***************************************************WEBGL 部分******************************************/
// 查询webgl模型列表
exports.ModelList = async ()=>{
	return await WEBGL.ModelList()
}



/***************************************************User 部分******************************************/
exports.Login = async (d)=> await User.Login(d)
exports.Register = async (d)=> await User.Register(d)
exports.GetUserInfo = async (d)=> await User.GetUserInfo(d)
exports.GetSts = async (d)=> await User.GetSts(d)
exports.SelUserList = async (d)=> await User.SelUserList(d)
exports.admin_SelUserList = async (d)=> await User.admin_SelUserList(d)
exports.admin_DelUser = async (d)=> await User.admin_DelUser(d)
exports.admin_UpdlUser = async (d)=> await User.admin_UpdlUser(d)

exports.UpdUser = async (d)=> await User.UpdUser(d)
exports.DelUser = async (d)=> await User.DelUser(d)



/***************************************************Permission 部分******************************************/
exports.SyncRouter = async (d)=> await Permission.SyncRouter(d)
exports.SelRouter = async (d)=> await Permission.SelRouter(d)
exports.AddRole = async (d)=> await Permission.AddRole(d)
exports.UpdRole = async (d)=> await Permission.UpdRole(d)
exports.DelRole = async (d)=> await Permission.DelRole(d)
exports.GetRoleInfo = async (d)=> await Permission.GetRoleInfo(d)



/***************************************************Update 部分******************************************/
exports.SelVersionList = async (d)=> await Update.SelVersionList(d)
exports.AddVersion = async (d)=> await Update.AddVersion(d)
exports.DelVersion = async (d)=> await Update.DelVersion(d)
exports.UpdVersion = async (d)=> await Update.UpdVersion(d)
exports.UpdCurrentUse = async (d)=> await Update.UpdCurrentUse(d)



/***************************************************Article 部分******************************************/
exports.SelArticleList = async (d)=> await Article.SelArticleList(d)
exports.UpdArticle = async (d)=> await Article.UpdArticle(d)
exports.DelArticle = async (d)=> await Article.DelArticle(d)



/***************************************************Other 部分******************************************/
exports.TestConsole = async (d)=> await Other.TestConsole(d)
exports.APICheckToken = async (d)=> await Other.APICheckToken(d)
exports.DelOssFile = async (d)=> await Other.DelOssFile(d)
