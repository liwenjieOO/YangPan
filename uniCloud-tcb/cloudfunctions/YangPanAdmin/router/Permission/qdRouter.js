const asyncRoutes = [
	/** ************************************************WebGl部分******************************************************************************/
	{
		path: '/WebGL',
		name: 'WebGL',
		meta: {
			title: 'WebGL',
			icon: '3D'
		},
		children: [{
				path: '/WebGL/ModelList',
				name: 'ModelList',
				meta: {
					title: '模型列表',
					icon: 'table'
				}
			},
			{
				path: '/WebGl/ModelShow',
				name: 'ModelShow',
				meta: {
					title: '模型展示',
					icon: 'UploadImg'
				}
			}
		]
	},

	/** ************************************************快速模板部分******************************************************************************/
	{
		path: '/fastTemplate',
		name: 'fastTemplate',
		meta: {
			title: '快速模板',
			icon: 'quick'
		},
		children: [{
				path: '/fastTemplate/index',
				name: 'fastTemplate-index',
				meta: {
					title: 'fastTemplate-index',
					icon: 'table'
				}
			},
			{
				path: '/fastTemplate/ImgUpload',
				name: 'fastTemplate-ImgUpload',
				meta: {
					title: '图片上传',
					icon: 'UploadImg'
				}
			},
			{
				path: '/fastTemplate/formValidate',
				name: 'fastTemplate-formValidate',
				meta: {
					title: '表单验证',
					icon: 'formValidate'
				}
			},
			{
				path: '/fastTemplate/Table',
				name: 'fastTemplate-Table',
				meta: {
					title: '表格',
					icon: 'table'
				}
			}
		]
	},

	/** ************************************************洋网盘后端部分******************************************************************************/
	{
		path: '/YangPan',
		name: 'AppUpdateList',
		meta: {
			title: '洋盘',
			icon: 'YangPan'
		},
		children: [{
			path: '/YangPan/AppUpdate/AppUpdateList',
			name: 'AppUpdateList',
			meta: {
				title: 'APP版本管理',
				icon: 'Version'
			}
		}, {
			path: '/YangPan/UserManager/UserList',
			name: 'UserManager',
			meta: {
				title: '用户管理',
				icon: 'Users'
			}
		}, {
			path: '/YangPan/ArticleManager/ArticleList',
			name: 'ArticleManager',
			meta: {
				title: '文章管理',
				icon: 'Article'
			}
		}, {
			path: '/YangPan/AuthorityManager/AuthorityManager',
			name: 'ArticleManager',
			meta: {
				title: '权限管理',
				icon: 'Article'
			}
		}]
	}
]

module.exports = {
	asyncRoutes
}
