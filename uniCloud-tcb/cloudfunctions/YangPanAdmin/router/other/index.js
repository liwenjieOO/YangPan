'use strict';
const db = uniCloud.database();
const dbCmd = db.command
const $ = db.command.aggregate
const tools = require("../../tools/index.js")
const alioss = require('../../oss/index.js')
const Encryption_decryption = require('../../JSEncrypt/index.js')

// 测试服务端打印接口
exports.TestConsole = async (body_data)=>{
	console.log("服务器打印的console：",body_data)
	return {aaaaaa:body_data}
}

// 检查Token是否存在或过期
exports.APICheckToken = async (Token)=>{
	// 查询数据
	let res = await db.collection('admin-User').where({Token}).get()
	if (res.data.length==0 || res.data ===null) { return {code: 211,msg: "Token已过期,请重新登录"} }
	// 数据存在
	if (res.data.length >= 1) { return {code: 200,msg: "身份校验通过",result:res} }
	return {msg: "内部错误~",code: 500}
}

// 根据文件名字删除-oss文件   （admin删除以用户头像）
exports.DelOssFile = async function(body_data){
	let res = {
		code:200,
		msg:"成功~"
	}
	let {UserInfo,filePath} = body_data
	if (!filePath) {return {msg: "filePath必传"}}
	
	let result =await alioss.deleteFile(filePath)	  
	if(result.res.status===204){
		res.msg='删除成功~'
		res.data=result.res
	}else{
		res.msg='内部错误~'
		res.code=500
	}
	return res
}