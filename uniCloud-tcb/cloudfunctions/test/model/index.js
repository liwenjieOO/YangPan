const db = uniCloud.database();
// 添加数据
exports.db_addOne = async (collect, data) => {
	let collection = db.collection(collect)
	let res = await collection.add(data)
	return res
}
// 查询数据
exports.db_select = async (collect, term) => {
	let res = term ? await db.collection(collect).where(term).get() : await db.collection(collect).get()
	return res
}
// 更新数据
exports.db_update = async (collect, _id, term) => db.collection(collect).doc(_id).update(term)

// 带条件更新数据
exports.db_update_where = async (collect, VVV, term) => db.collection(collect).where({'fileId':VVV}).update(term)

// 删除数据
exports.db_delete = async (collect, _id, term) => db.collection(collect).doc(_id).remove()