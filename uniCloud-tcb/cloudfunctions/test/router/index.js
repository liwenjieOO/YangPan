var UpdateS = require('./Update/index.js')
var fileS = require('./file/index.js')
var homeS = require('./home/index.js')
var communityS = require('./community/index.js')
var noticeS = require('./notice/index.js')
var HuanXinIMS = require('./HuanXinIM/index.js')
var StudyPlaneS = require('./StudyPlane/index.js')


/*
	@更新模块
*/
// 
exports.APICheckToken = UpdateS.APICheckToken
exports.CheckUpdate = UpdateS.CheckUpdate
exports.SelUpdateList = UpdateS.SelUpdateList
exports.SelNewApk = UpdateS.SelNewApk



/*
	@文件模块
*/
// 
exports.APINewFolder = fileS.APINewFolder
exports.APIFreshSts = fileS.APIFreshSts
exports.APIGetMpUploadOssHelper = fileS.APIGetMpUploadOssHelper
exports.APIUploadFile = fileS.APIUploadFile
exports.APISyncUploadFile = fileS.APISyncUploadFile
exports.APIDelUploadRecord = fileS.APIDelUploadRecord
exports.APIGetUploadRecordList = fileS.APIGetUploadRecordList
exports.APIGetFolderList = fileS.APIGetFolderList
exports.APIDelFile = fileS.APIDelFile
exports.APIUploadImg = fileS.APIUploadImg
exports.APIUploadImgSync = fileS.APIUploadImgSync
exports.getSpecifyTypeFile = fileS.getSpecifyTypeFile
exports.getSpecifiedFileSizes = fileS.getSpecifiedFileSizes



/*
	@我的模块
*/
// 用户界面
exports.getSMSInterface = homeS.getSMSInterface
exports.APIRegister = homeS.APIRegister
exports.APIRegister_thirdparty = homeS.APIRegister_thirdparty
exports.APILogin = homeS.APILogin
exports.APIGetUserinfo = homeS.APIGetUserinfo
exports.APIUpdUserinfo = homeS.APIUpdUserinfo
exports.APIUpdatePwd = homeS.APIUpdatePwd
exports.CheckVerCode = homeS.CheckVerCode




/*
	@社区模块
*/
// 文章
exports.PushArticle = communityS.PushArticle
exports.DelArticle = communityS.DelArticle
exports.SelArticleList = communityS.SelArticleList
exports.SelVideoList = communityS.SelVideoList
exports.SelArticleDetail = communityS.SelArticleDetail
exports.SelPersionPublish = communityS.SelPersionPublish
exports.SelArticle_like = communityS.SelArticle_like
exports.UploadRecord = communityS.UploadRecord
// 评论
exports.onLike = communityS.onLike
exports.onCommon = communityS.onCommon
exports.SelCommonList = communityS.SelCommonList
exports.CommonLike = communityS.CommonLike
exports.CommonSort = communityS.CommonSort



/*
	@通知模块
 */
exports.addNotice = noticeS.addNotice
exports.SelNotice = noticeS.SelNotice
exports.ReadedNotice = noticeS.ReadedNotice
exports.SelNoReadNoticeCount = noticeS.SelNoReadNoticeCount
exports.DelNotice = noticeS.DelNotice




/*
	@聊天模块
 */
exports.SelNickNameAvar = HuanXinIMS.SelNickNameAvar
exports.SelPwd = HuanXinIMS.SelPwd

/*
	@学习计划模块
 */
exports.AsyncStyudyPlaneData = StudyPlaneS.AsyncStyudyPlaneData
exports.DelStyudyPlaneData = StudyPlaneS.DelStyudyPlaneData
