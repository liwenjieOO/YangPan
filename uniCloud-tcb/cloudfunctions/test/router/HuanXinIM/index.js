'use strict';
const db = uniCloud.database();
const dbCmd = db.command
const $ = db.command.aggregate
const tools = require("../../tools/index.js")
const alioss = require('../../oss/index.js')
const noticeS = require('../notice/index.js')

// 注册环信IM账号
let registerHuanXinIm = async(body_data)=>{
	let returnData = { code: 200,msg: "参数错误~" }
	let {_id,pwd} = body_data
	
	return returnData
}

// 通过_id号查询账号昵称和头像
exports.SelNickNameAvar = async(body_data)=>{
	let returnData = { code: 200,msg: "参数错误~" }
	let {_id} = body_data
	if(!_id) return returnData
	let res = await db.collection('User').where({_id}).field({
		'avatar':true,
		"Nickname":true,
		"_id":true
	}).get()
	returnData.msg = '查询成功~'
	returnData.data = res.data[0]
	return returnData
}

// 通过_id号查询密码 用户切换账号  环信IM的登录
exports.SelPwd = async(body_data)=>{
	let returnData = { code: 200,msg: "参数错误~" }
	let {_id} = body_data
	if(!_id) return returnData
	let res = await db.collection('User').where({_id}).field({
		"pwd":true,
		"openid":true
	}).get()
	returnData.msg = '查询成功~'
	returnData.data = res.data[0]
	return returnData
}
