'use strict';
const UserS = require('./User.js')

// 用户界面
exports.getSMSInterface = UserS.getSMSInterface
exports.APIRegister = UserS.APIRegister
exports.APIRegister_thirdparty = UserS.APIRegister_thirdparty
exports.APILogin = UserS.APILogin
exports.APIGetUserinfo = UserS.APIGetUserinfo
exports.APIUpdUserinfo = UserS.APIUpdUserinfo
exports.APIUpdUserAva = UserS.APIUpdUserAva
exports.APIUpdatePwd = UserS.APIUpdatePwd
exports.CheckVerCode = UserS.CheckVerCode

