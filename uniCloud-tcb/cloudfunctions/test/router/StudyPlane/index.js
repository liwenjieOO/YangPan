'use strict';
const db = uniCloud.database();
const dbCmd = db.command
const $ = db.command.aggregate
const tools = require("../../tools/index.js")
const alioss = require('../../oss/index.js')

// 同步学习记录接口（需要鉴权）
exports.AsyncStyudyPlaneData = async(body_data)=>{
	let returnData = { code: 200,msg: "同步成功~" }
	
	/*
		同步思路：是一个双向的过程
			同步接口先接受前端的本地数据过来，然后取出数据库的数据，然后进行比较，时间戳相同就合并，不相同就添加，然后返回数据库的数据给前端
	*/
	let {Datas,UserInfo} = body_data
	if(!Datas || Datas.length===0){
		let Res = await db.collection('StudyPlane').field({_id:false,UserId:false}).where({UserId:UserInfo._id}).get() // 一个人的全部学习计划
		returnData.data = Res.data
		return returnData  // 这边要返回全部的数据哦~
	} 
	
	try{
		let affair = await db.runTransaction(async transaction => {
			
			let AllPlaneRes = await transaction.collection('StudyPlane').where({UserId:UserInfo._id}).get() // 一个人的全部学习计划
			if(AllPlaneRes.data.length===0){ // 纯全部添加
				for(let i=0;i<Datas.length;i++){ Datas[i]['UserId'] = UserInfo._id }  //数据拼凑
				let addRes = await transaction.collection('StudyPlane').add(Datas)
				if(addRes.ids.length!==Datas.length){await transaction.rollback(`插入错误${JSON.stringify(addRes)},${Datas.length}`)}
				returnData.msg='插入成功~'
				// let res = await transaction.collection('StudyPlane').field({_id:false,UserId:false}).where({UserId:UserInfo._id}).get() // 一个人的全部学习计划
				// returnData.data= res.data;  // TODO bushi 这个数据
				return returnData
			}else{
				let opterateCout =0 // 用来记录操作成功次数~
				// 暴力算法
				for(let j=0;j<Datas.length;j++){
					let indexs = AllPlaneRes.data.findIndex(i=>i.LocalId===Datas[j].LocalId)
					if(indexs === -1){ // 添加一整条
						Datas[j]['UserId'] = UserInfo._id
						let AddRes =  await transaction.collection('StudyPlane').add(Datas[j])
						if(AddRes.id) opterateCout++ // 成功数+1
					}else{ // 修改单条
						let ServerData = AllPlaneRes.data[indexs]
						// 合并 首先是以数据库的为准是吧
						ServerData.title = Datas[j].title
						let FatherArr = [0,0,0];
						Datas[j].Child.forEach((ii,jj)=>{
							let ChildIndexs = ServerData.Child.findIndex(item=>item.LocalId===ii.LocalId)
							if(ChildIndexs === -1){  // 添加一整条Child
								ServerData.Child.push(ii)
								// console.log('我有被添加进去了哦')
							}else{
								// 这一条记录是不可能相等的差点忘了我操
								// ServerData.Child[ChildIndexs].title = ii.title
								// ServerData.Child[ChildIndexs].OpearteTime = ii.OpearteTime
								// ServerData.Child[ChildIndexs].times = ii.times
							}
							let ChildArr = ii.StudyTime.split('.').map(Number);
							FatherArr[1] = FatherArr[1] + ChildArr[0];
							FatherArr[2] = FatherArr[2] + ChildArr[1];
							// 分钟进时钟  --  时钟进天数
							FatherArr[1] = FatherArr[1] + Math.floor(FatherArr[2] / 60);
							FatherArr[2] = FatherArr[2] % 60
							FatherArr[0] = FatherArr[0] + Math.floor(FatherArr[1] / 24)
							FatherArr[1] = FatherArr[1] % 24
						})
						ServerData.times = FatherArr.join('.') // 这个肯定要重新计算的
						let ___id = ServerData._id
						delete ServerData._id
						let UpdateRes = await transaction.collection('StudyPlane').doc(___id).update(ServerData)
						// console.log(UpdateRes)
						// if(UpdateRes.updated>0) opterateCout++ // 成功数+1
					}				
				}
				// let Res = await transaction.collection('StudyPlane').field({_id:false,UserId:false}).where({UserId:UserInfo._id}).get() // 一个人的全部学习计划
				return {
					code:200,
					msg:"同步成功~"
				}
			}
		})
		let Res = await db.collection('StudyPlane').field({_id:false,UserId:false}).where({UserId:UserInfo._id}).get() // 一个人的全部学习计划
		affair.data = Res.data
		return affair
	}catch(e){
		console.log('内部错误~',e)
		returnData.code=500
		returnData.msg='内部错误'
		return returnData
	}
}

// 删除计划条(整条)
exports.DelStyudyPlaneData = async(body_data)=>{
	let returnData = { code: 200,msg: "参数错误~" }
	let {LocalId,UserInfo} = body_data
	if(!LocalId) return returnData
	let res = await db.collection('StudyPlane').where({LocalId,UserId:UserInfo._id}).remove()
	
	returnData.msg='删除成功~'
	return returnData
}