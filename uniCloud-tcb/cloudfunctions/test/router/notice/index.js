'use strict';
const db = uniCloud.database();
const dbCmd = db.command
const $ = db.command.aggregate
const tools = require("../../tools/index.js")
const alioss = require('../../oss/index.js')

// 添加通知信息
exports.addNotice = async(body_data)=>{
	let returnData = { code: 200,msg: "参数错误~" }
	
	let {noticeUserId,noticeArticleId,noticeType,SourceUserId,noticeCommonId} = body_data
	if(!noticeUserId || !noticeType || !SourceUserId) return returnData
	
	let SelJson = {noticeUserId,noticeArticleId,noticeType,SourceUserId,noticeCommonId,IsRead:false}
	
	try{
		let affair = await db.runTransaction(async transaction => {
			let IsExistence = await transaction.collection('notice').where(SelJson).get() // 是否存在过
			let res;
			if(IsExistence.data.length>0 && (noticeType === 1 || noticeType===3 || noticeType===5 || noticeType===7)){ // 存在 覆盖  && 只有点赞覆盖
				// res = await transaction.collection('notice').doc(IsExistence.data[0]._id).update({CreatTime:db.serverDate()})
			}else{ // 不存在创建
				res = await transaction.collection('notice').add(Object.assign(SelJson,{CreatTime:db.serverDate(),Ip: __ctx__.CLIENTIP}))
			}
			return res
		})
		returnData.msg='插入成功~'
		returnData.data = affair
		return returnData
	}catch(e){
		console.log('内部错误~')
		returnData.code=500
		returnData.msg='内部错误'
		return returnData
	}
	
}

// 查询通知信息
exports.SelNotice = async(body_data)=>{
	let returnData = { code: 200,msg: "参数错误~" }
	let {pageSize,pageNum,noticeType,UserInfo} = body_data
	if(!noticeType) return returnData
	let Skips = pageSize * pageNum
	
	let SelJson = {
		noticeUserId:UserInfo._id,
		noticeType:dbCmd.or([dbCmd.eq(1),dbCmd.eq(2),dbCmd.eq(3),dbCmd.eq(4),dbCmd.eq(5),dbCmd.eq(6),dbCmd.eq(7)]),
		SourceUserId:dbCmd.neq(UserInfo._id)
	}
	
	try{
		let affair = await db.runTransaction(async transaction => {
			let total = await transaction.collection('notice').where(SelJson).count()
			let res = await transaction.collection('notice').aggregate().match(SelJson).lookup({
				from:'User',
				localField:'SourceUserId',
				foreignField:'_id',
				as:"UserInfo"
			}).lookup({
				from:'common',
				localField:'noticeCommonId',
				foreignField:'_id',
				as:"CommonInfo"
			}).sort({CreatTime:-1}).skip(Skips).limit(pageSize).end()
			return {
				total:total,
				data:res.data
			}
		})
		returnData.msg='查询成功~'
		returnData.data = affair.data
		returnData.total = affair.total.total
		return returnData
	}catch(e){
		console.log('内部错误~')
		returnData.code=500
		returnData.msg='内部错误'
		return returnData
	}
}

// 查询通知未读信息统计
exports.SelNoReadNoticeCount = async(body_data)=>{
	let returnData = { code: 200,msg: "参数错误~" }
	let {UserInfo} = body_data
	try{
		let affair = await db.runTransaction(async transaction => {
			let res = await db.collection('notice').where({
				noticeUserId:UserInfo._id,IsRead:false,
				SourceUserId:dbCmd.neq(UserInfo._id) // 不查自己点赞啥的
			}).count()
			return res.total
		})
		returnData.msg='查询成功~'
		returnData.total = affair
		return returnData
	}catch(e){
		console.log('内部错误~')
		returnData.code=500
		returnData.msg='内部错误'
		return returnData
	}
}

// 完成阅读通知
exports.ReadedNotice = async(body_data)=>{
	let returnData = { code: 200,msg: "参数错误~" }
	// _id 是通知信息的id
	let {_id} = body_data
	if(!_id) return returnData
	let res = await db.collection('notice').doc(_id).update({
		IsRead:true
	})
	if(res.updated>0){
		returnData.msg = '完成阅读通知成功！'
		return returnData
	}else{
		returnData.code = 200
		returnData.msg = '操作未成功~'
		return returnData
	}
	
}

// 删除通知信息
exports.DelNotice = async(body_data)=>{
	let returnData = { code: 200,msg: "参数错误~" }
	let {_id} = body_data
	if(!_id) return returnData
	
	try{
		let affair = await db.runTransaction(async transaction => {
			let res = await db.collection('notice').doc(_id).remove()
			return res
		})
		returnData.msg='删除成功~'
		returnData.total = affair
		return returnData
	}catch(e){
		console.log('内部错误~')
		returnData.code=500
		returnData.msg='内部错误'
		return returnData
	}
}