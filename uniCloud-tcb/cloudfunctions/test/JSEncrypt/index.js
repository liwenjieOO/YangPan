const PPP = require('ppp');
const $JSEncrypt = new PPP.jsencrypt();


let pubKey =
	`-----BEGIN PUBLIC KEY-----
		MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCxaE0pR7Kw/LnRk2QhzN+kkwKT
		RrimPxO7PqH7TcQ1mp94HtCVj1WVSIosZsS5N1DDJgKDOIbQS04Tq+J47/VpaW9H
		MAXSjZwQ03euN5Z7a0v/8H/aXw4OJgzdBPillwaahaUAlpQjJXIlGvslIw93f5uE
		eNRvG9otLKTBlz+tPwIDAQAB
		-----END PUBLIC KEY-----`

let CertificatePassword = 'YangPan'

let privateKey =
	`-----BEGIN PRIVATE KEY-----
		MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALFoTSlHsrD8udGT
		ZCHM36STApNGuKY/E7s+oftNxDWan3ge0JWPVZVIiixmxLk3UMMmAoM4htBLThOr
		4njv9Wlpb0cwBdKNnBDTd643lntrS//wf9pfDg4mDN0E+KWXBpqFpQCWlCMlciUa
		+yUjD3d/m4R41G8b2i0spMGXP60/AgMBAAECgYAFD+LTwLX6ktuCfWAoDr565a73
		INd39ujLOPhKQWfN18GhHc/51IMupUk8Bxqp5kgpqVX9BInkO6HYsdjedopcEWnD
		rERl/gvbT2iC1jYmGKfuQBR3D3YLPXrrQWSacIDAcizMa5uEHWKWkwV7n7Wuftkc
		K94buheKB9jyt45GMQJBAOyDu1RfkFGwcgwv0WfhK3ALxHwTZUfARP2fnoN9L868
		sukjnlrSyzoDYY56xMLQAAxp/S4/bhbLfdM5DvSxMYMCQQDABfS3xbW3Jk0DilyT
		VoleXJVCOI2+KzD992ks5f1vJEPBBhf6ma5Q6kB0IC96FHPRbv5CVHGF16KQl/nA
		z/SVAkEAgZkhYfdHdmuv4pulLD3G9ouMK8zY93sUksEPM8+6DZd6laON1wigkXZe
		05rUDEe7bxH4yIm7QkRY+w/N8SjTDwJAUsu+ouaq3Ze0KUylchokDP3alb0Q0OiK
		rVbakLNyPVPjqvzXpgRRty3qMKgYEIl1Dt7O3wbOFCnQPIxq8go4qQJBAOfrsGol
		JW+DnGDCHxOmwg2VywA8jigFQh90N2fAMLfK3baQfnqyhCL2VHYY6OT0ZB05pybM
		YyJp0EDFV2X4FNY=
		-----END PRIVATE KEY-----`


$JSEncrypt.setPrivateKey(privateKey)

module.exports = {
	$JSEncrypt, // 加密解密实例
	pubKey,
	CertificatePassword,
	privateKey
}
