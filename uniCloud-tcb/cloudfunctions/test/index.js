'use strict';
const router = require('./router/index.js');
// 接口名称校验（全部接口）
const Router = [
	// 更新模块
	'APICheckToken', 'CheckUpdate','SelUpdateList','SelNewApk',
	// 文件模块
	'APINewFolder', 'APIFreshSts','APIGetMpUploadOssHelper','APIUploadFile','APISyncUploadFile', 'APIDelUploadRecord',
	'APIGetUploadRecordList','APIGetFolderList', 'APIDelFile', 'APIUploadImg','APIUploadImgSync', 'getSpecifyTypeFile','getSpecifiedFileSizes',
	// 用户
	'getSMSInterface','APIRegister','APIRegister_thirdparty','APILogin','APIGetUserinfo','APIUpdUserinfo','APIUpdatePwd','CheckVerCode',
	// 文章
	'PushArticle','DelArticle', 'SelArticleList', 'SelVideoList','SelArticleDetail', 'SelPersionPublish','SelArticle_like','UploadRecord',
	// 评论
	'onLike', 'onCommon','SelCommonList', 'CommonLike','CommonSort',
	// 通知模块
	'addNotice','SelNotice','ReadedNotice','SelNoReadNoticeCount','DelNotice',
	// 聊天模块
	'SelNickNameAvar','SelPwd',
	// 效率管理模块
	"AsyncStyudyPlaneData",'DelStyudyPlaneData'
];
// 不需要tokoen的接口
const NoToken = [
	// 用户
	'APILogin','APIRegister','APIRegister_thirdparty','getSMSInterface','APIUpdatePwd','CheckVerCode',
	// 更新模块
	'CheckUpdate','SelNewApk'
]
// 检查接口是否存在
const CheckRouter = (API) => {
	let len = Router.filter(i => i == API).length
	return len >= 1 ? true : false
}

// 主云函数
exports.main = async (event, context) => {
	console.log('函数日志输出================');
	console.log("event:", event);
	if (!event.httpMethod) {
		/*********************************************客户端调云函数基础用法*********************************************/
		const {
			type
		} = event;
		if (!CheckRouter(type)) {
			return {
				msg: "接口不存在，请参考文档"
			}
		};

		function eventType(type) {
			this.type = type
		}
		eventType.prototype.APICheckUpdate = (d) => APICheckUpdate(d)
		let router_init = new eventType(type)
		return router_init[type](event)

	} else {
		/*********************************************使用云函数URL调法**********************************************************/
		const {httpMethod,headers} = event
		if (httpMethod == 'POST') {
			const Token = headers.token || null
			// 数据处理
			const body_data = event.body ? JSON.parse(event.body) : 'error' // string转一次
			// 接口类型校验
			let {type} = body_data
			if (!CheckRouter(type)) { return {msg: "接口不存在，请参考文档"} }
			body_data['Token'] = Token
			
			// 过滤掉不需要token的接口
			if(NoToken.find(i=>i==type)===undefined){
				let IdentityCheck = await router.APICheckToken(Token)
				if(IdentityCheck.code == 211){ return {code:IdentityCheck.code,msg:IdentityCheck.msg} }
				body_data['UserInfo'] = IdentityCheck.result.data[0]
			}
			
			// 路由注册
			function eventType(type) {this.type = type}
			
			
			
			/*
				@更新模块
					methods：
						APICheckToken: 检查Token是否存在或过期
						CheckUpdate: 检查版本更新
						SelUpdateList: 检查所有历史更新版本
						SelNewApk: 获取最新版本的更新包
			*/
			eventType.prototype.APICheckToken = (Token) => router.APICheckToken(Token)
			eventType.prototype.CheckUpdate = (d) => router.CheckUpdate(d)
			eventType.prototype.SelUpdateList = (d) => router.SelUpdateList(d)
			eventType.prototype.SelNewApk = (d) => router.SelNewApk(d)
			
			
			/*
				@文件模块
					methods：
						APINewFolder: 创建文件夹接口
						APIFreshSts: 刷新sts接口
						APIGetMpUploadOssHelper: 获取签证接口(上传文件用)
						APIUploadFile: 上传接口（初始化记录调用的）
						APISyncUploadFile: 上传接口（同步记录调用的）
						APIDelUploadRecord:  删除上传记录（删除安卓设备的） //TODO  后期接口优化 上传设备信息
						APIGetUploadRecordList: 获取上传记录
						APIGetFolderList: 获取文件列表
						APIDelFile: 删除文件 （算法）
						APIUploadImg: 获取上传图片签名接口
						APIUploadImgSync: 写入上传图片记录(同步,上传成功才调用)
						getSpecifyTypeFile: 获取该账户下全部指定文件类型
						getSpecifiedFileSizes: 获取指定目录的文件大小
			*/
			eventType.prototype.APINewFolder = (d) => router.APINewFolder(d)
			eventType.prototype.APIFreshSts = (d) => router.APIFreshSts(d)
			eventType.prototype.APIGetMpUploadOssHelper = (d) => router.APIGetMpUploadOssHelper(d)
			eventType.prototype.APIUploadFile = (d) => router.APIUploadFile(d)
			eventType.prototype.APISyncUploadFile = (d) => router.APISyncUploadFile(d)
			eventType.prototype.APIDelUploadRecord = (d) => router.APIDelUploadRecord(d)
			eventType.prototype.APIGetUploadRecordList = (d) => router.APIGetUploadRecordList(d)
			eventType.prototype.APIGetFolderList = (d) => router.APIGetFolderList(d)
			eventType.prototype.APIDelFile = (d) => router.APIDelFile(d)
			eventType.prototype.APIUploadImg = (d) => router.APIUploadImg(d)
			eventType.prototype.APIUploadImgSync = (d) => router.APIUploadImgSync(d)
			eventType.prototype.getSpecifyTypeFile = (d) => router.getSpecifyTypeFile(d)
			eventType.prototype.getSpecifiedFileSizes = (d) => router.getSpecifiedFileSizes(d)
			
			
			/*
				@我的模块
					methods：
						getSMSInterface: 获取短信验证码
						APIRegister: 注册接口
						APIRegister_thirdparty: 第三方注册/登录(包括登录附加STS权限)
						APILogin: 登录接口（密码登录/短信验证码登录）
						APIGetUserinfo: 获取个人信息
						APIUpdUserinfo: 修改个人信息
						APIUpdatePwd: 修改个人密码
						CheckVerCode: 校验验证码是否正确
			*/
			eventType.prototype.getSMSInterface = (d) => router.getSMSInterface(d)
			eventType.prototype.APIRegister = (d) => router.APIRegister(d)
			eventType.prototype.APIRegister_thirdparty = (d) => router.APIRegister_thirdparty(d)
			eventType.prototype.APILogin = (d) => router.APILogin(d)
			eventType.prototype.APIGetUserinfo = (d) => router.APIGetUserinfo(d)
			eventType.prototype.APIUpdUserinfo = (d) => router.APIUpdUserinfo(d)
			eventType.prototype.APIUpdatePwd = (d) => router.APIUpdatePwd(d)
			eventType.prototype.CheckVerCode = (d) => router.CheckVerCode(d)
			
			
			/*
				@社区模块------文章
					methods：
						PushArticle: 发布文章
						DelArticle: 删除文章
						SelArticleList: 获取文章列表
						SelVideoList: 获取视频列表（未作）
						SelArticleDetail: 查询当前文章详细信息(这边好像是来加载评论/点赞/观看数的)
						SelPersionPublish: 查询个人发布
						SelArticle_like: 文章的模糊搜索
						UploadRecord: 上传文件记录(文章上传音频是放在oss上的，以后包括视频要放在oss上)
			*/
			eventType.prototype.PushArticle = (d) => router.PushArticle(d)
			eventType.prototype.DelArticle = (d) => router.DelArticle(d)
			eventType.prototype.SelArticleList = (d) => router.SelArticleList(d)
			eventType.prototype.SelVideoList = (d) => router.SelVideoList(d)
			eventType.prototype.SelArticleDetail = (d) => router.SelArticleDetail(d)
			eventType.prototype.SelPersionPublish = (d) => router.SelPersionPublish(d)
			eventType.prototype.SelArticle_like = (d) => router.SelArticle_like(d)
			eventType.prototype.UploadRecord = (d) => router.UploadRecord(d)
			
			
			/*
				@社区模块------评论
					methods：
						onLike: 单条评论点赞
						onCommon: 评论接口  (一级和二级一起)
						SelCommonList: 查询评论接口（一级/二级评论）
						CommonLike: 单条评论点赞
						CommonSort: 评论排序（已经废弃）
			*/
			eventType.prototype.onLike = (d) => router.onLike(d)
			eventType.prototype.onCommon = (d) => router.onCommon(d)
			eventType.prototype.SelCommonList = (d) => router.SelCommonList(d)
			eventType.prototype.CommonLike = (d) => router.CommonLike(d)
			eventType.prototype.CommonSort = (d) => router.CommonSort(d)
			
			
			/*
				@通知中心
				methods：
					addNotice: 添加通知信息
					SelNotice: 查询通知信息
					ReadedNotice: 阅读完通知
					SelNoReadNoticeCount: 查询通知未读信息统计
					DelNotice: 删除通知信息
			 */
			eventType.prototype.addNotice = (d) => router.addNotice(d)
			eventType.prototype.SelNotice = (d) => router.SelNotice(d)
			eventType.prototype.ReadedNotice = (d) => router.ReadedNotice(d)
			eventType.prototype.SelNoReadNoticeCount = (d) => router.SelNoReadNoticeCount(d)
			eventType.prototype.DelNotice = (d) => router.DelNotice(d)
			
			
			/*
				@学习效率
				methods：
					AsyncStyudyPlaneData: 同步数据到数据库
					DelStyudyPlaneData:  删除计划目标大条的
			 */
			eventType.prototype.AsyncStyudyPlaneData = (d) => router.AsyncStyudyPlaneData(d)
			eventType.prototype.DelStyudyPlaneData = (d) => router.DelStyudyPlaneData(d)
			
			
			/*
				@聊天
				methods：
					SelNickNameAvar: 通过_id查询用户的信息
					SelPwd:   通过_id查询用户的密码
			 */
			eventType.prototype.SelNickNameAvar = (d) => router.SelNickNameAvar(d)
			eventType.prototype.SelPwd = (d) => router.SelPwd(d)  // 要toke的
			
			

			let router_init = new eventType(type)
			return router_init[type](body_data)

		} else if (httpMethod == 'GET') { //GET接口
			let {
				type
			} = event.queryStringParameters
			if (!CheckRouter(type)) {
				return {
					msg: "接口不存在，请参考文档"
				}
			}
		}
	}

};
