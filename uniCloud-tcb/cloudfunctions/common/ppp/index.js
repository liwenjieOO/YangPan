const OSS = require('ali-oss');
const crypto = require("crypto-js");
const jsencrypt = require("node-jsencrypt");

PPP = {
	OSS,
	crypto,
	jsencrypt
}
module.exports = PPP;
