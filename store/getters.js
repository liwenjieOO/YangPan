const getters = {
	File_Get_CurrentPath: state => state.file.CurrentPath,
	File_Get_OssClient: state => state.file.OssClient, // Oss对象
	File_Get_DownloadTaskt: state => state.file.DownloadTask, // 正在下载任务队列
	File_Get_UploadTask: state => state.file.UploadTask, // 正在上次任务队列
	File_Get_CompleteDownloadTask: state => state.file.CompleteDownloadTask, //下载完成队列
	File_Get_CompleteUploadTask: state => state.file.CompleteUploadTask, // 上传完成队列

	GET_innerAudioContext: state => state.Audio.innerAudioContext, // 唯一 audio 上下文
	GET_AudioFileID: state => state.Audio.fileID, // 用来控制单一播放音频
	
	
	
	
	Get_CurrentMode: state => state.theme.CurrentMode, // 白天/暗黑模式
	
	
	
	
	
	
	
	
	
	
	


	GetFileBarState: state => state.FileBarState, // 文件操作栏显示状态
	GetTarBarState: state => state.TarBarState, // tarbar显示状态
	GetPopupShow: state => state.PopupShow, // 添加按钮和传输记录按钮的显示状态
	GetPopupPositon: state => state.PopupPositon, // 弹窗动画效果
	GetPopupState: state => state.PopupState, // 弹窗内容
	GetPopupActive: state => state.PopupActive, // transfer-popup组件的tag页标签
	GetOssClient: state => state.OssClient, // oss实例化对象

	GetFileCurrentPath: state => state.file.FileCurrentPath, // 当前的目录
	GetShow_NewFolder: state => state.file.Show_NewFolder, // 是否显示新建文件夹
	GetCompleteduploadTask: state => state.file.CompleteduploadTask, // 已完成的文件上传队列
	GetUndoneuploadTask: state => state.file.UndoneuploadTask, // 未完成的文件上传队列
	GetCompletedDownLoadTask: state => state.file.CompletedDownLoadTask, // 已完成的文件下载队列
	GetUndoneDownLoadTask: state => state.file.UndoneDownLoadTask, // 未完成的文件下载队列
	GetSelectList: state => state.file.SelectList, // 选中文件数组(check)
	GetCxyFileBar_cont_MainContent: state => state.file.CxyFileBar_cont_MainContent, // 组件的联调控制
	GetCxyFileBar_cont_File_index: state => state.file.CxyFileBar_cont_File_index, // 组件的联调控制

}
export default getters
