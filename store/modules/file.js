import $pub from '../../common/public/index.js';
import {API$GetSts} from '../../common/API/file.js'
import AliOos from 'ali-oss' 
export default {
	state: {
		CurrentPath:"" ,// 文件主页 当前的文件路径
		OssClient:null ,// 实例化的oss对象
		StsConfig:{// sts凭证
			region: 'oss-cn-hangzhou',
			accessKeyId:$pub.getStorageSync('Sts').credentials ? $pub.getStorageSync('Sts').credentials.AccessKeyId:"1111111111111111111111111111111111111111111",
			accessKeySecret:$pub.getStorageSync('Sts').credentials ? $pub.getStorageSync('Sts').credentials.AccessKeySecret:"1111111111111111111111111111111111111111111",
			stsToken:$pub.getStorageSync('Sts').credentials ? $pub.getStorageSync('Sts').credentials.SecurityToken:"1111111111111111111111111111111111111111111",
			bucket: 'qdds666'
		},
		DownloadTask:[] ,			// 下载队列
		CompleteDownloadTask:[] ,	// 下载完成队列
		UploadTask:[] ,				// 上传队列
		CompleteUploadTask:[] 		// 上传完成队列
	},
	mutations: {
		// 改变 当前路径
		CHANGE_CurrentPath(state, value) {
			state.CurrentPath = value
		},
		// 添加 下载队列
		ADD_DownloadTask(state, value){
			state.DownloadTask.unshift(value)
		},
		// 添加 上传队列
		ADD_UploadTask(state, value){
			state.UploadTask.unshift(value)
		},
		// 删除 下载队列
		DEL_DownloadTask(state, id){
			for(let i=0,item;item=state.DownloadTask[i];i++){
				if(item.id==id){
					state.DownloadTask.splice(i,1)
					break
				}
			}
		},
		// 删除 上传队列
		DEL_UploadTask(state, id){
			for(let i=0,item;item=state.UploadTask[i];i++){
				if(item.id==id){
					state.UploadTask.splice(i,1)
					break
				}
			}
		},
		// 添加 下载完成队列(根据下载的队列ID寻找添加)
		ADD_CompleteDownloadTask_id(state, id){
			for(let i=0,item;item=state.DownloadTask[i];i++){
				if(item.id==id){
					state.CompleteDownloadTask.unshift(state.DownloadTask.slice(i,i+1)[0])
					break
				}
			}
		},
		// 添加 上传完成队列  全部赋值
		CHANGE_CompleteUploadTask(state, arr){
			state.CompleteUploadTask = arr
		},
		// 添加 上传完成队列(根据上传的队列ID寻找添加)并清空掉上传中的数据
		ADD_CompleteUploadTask_id(state, id){
			for(let i=0,item;item=state.UploadTask[i];i++){
				if(item.id==id){
					let DelObj = state.UploadTask.splice(i,1)[0]
					state.CompleteUploadTask.unshift(DelObj)
					console.log(DelObj)
					break
				}
			}
		},
		// 取消上传任务
		stopUploadTask(state, id){
			for(let i=0,item;item=state.UploadTask[i];i++){
				if(item.id==id){
					item.uploadTask.abort()
					state.UploadTask.splice(i,1)
					break
				}
			}
		}
	},
	actions: {
		// 创建阿里oss对象
		Create_AliOssClient({ state }){
			console.log(state.StsConfig)
			state.OssClient = new AliOos(state.StsConfig)
		},
		// 更新Sts凭证
		async RefresgSts({ state }){
			let {code,data} =  await API$GetSts()
			if(code==200){
				$pub.setStorageSync('Sts', data);
			}
			state.StsConfig = {// sts凭证
				region: 'oss-cn-hangzhou',
				accessKeyId:data.credentials.AccessKeyId,
				accessKeySecret:data.credentials.AccessKeySecret,
				stsToken:data.credentials.SecurityToken,
				bucket: 'qdds666'
			}
		},
	}
}
