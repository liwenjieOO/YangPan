import $pub from '../../common/public/index.js';
import {
	API$GetSts
} from '../../common/API/file.js'
import AliOos from 'ali-oss'
export default {
	state: {
		innerAudioContext: "", // 创建并返回内部 audio 上下文
		fileID :'fitstLoad'	// 用来控制单一播放音频
	},
	mutations: {
		// 改变 audio 上下文的路径
		CHANGE_innerAudioContext(state, value) {
			console.log(' 改变 audio 上下文的路径')
			state.innerAudioContext = value
		},
		// 改变 fileID
		CHANGE_fileID(state, value) {
			console.log('改变 fileID')
			state.fileID = value
		},
		// 播放录音
		PlayAudio(state){
			console.log('播放录音')
			state.innerAudioContext.play()
		},
		// 暂停录音
		PauseAudio(state){
			console.log('暂停录音')
			state.innerAudioContext.pause()
		},
		// 销毁录音
		DestroyAudio(state){
			console.log('销毁录音')
			state.innerAudioContext.destroy()
		},
		// 设置Audio的Src
		SetAudioSrc(state,value){
			console.log('设置Audio的Src')
			state.innerAudioContext.src=value
		}
	},
	actions: {

	}
}
