export default {
	state: {
		// CurrentMode:{  // 当前选择的模式
		// 	name:"DarkMode",
		// 	color:"#ffffff",
		// 	backgroundColor:"#000000"
		// }
		CurrentMode:{  // 当前选择的模式
			name:"LightMode",
			color:"#000000",
			navColor:"#87ceeb",
			backgroundColor:"#fff"
		}
	},
	mutations: {
		// 改变 模式
		Set_CurrentMode(state, value) {
			state.CurrentMode = JSON.parse(JSON.stringify(value))
		}
	},
	actions: {

	}
}
