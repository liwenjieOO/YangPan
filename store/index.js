import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters.js'
import index from './modules/index.js'
import file from './modules/file.js'
import share from './modules/share.js'
import home from './modules/home.js'
import other from './modules/other.js'
import Audio from './modules/Audio.js'
import theme from './modules/theme.js'

Vue.use(Vuex)
const store = new Vuex.Store({
	state:{},
	mutations:{},
	actions:{},
	getters,
	modules:{
		file,
		other,
		Audio,
		theme
	}
})
export default store